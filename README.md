<div dir="rtl" align="right" style="color:blue"><h1>بنام خدا</h1></div>

[toc]

# نکته کنکوری

در `Ansible` ما یک `ansible_user` داریم،یک `become_user` . اولی جزو پارامترای اضافی می باشد که در خط فرمان توسط کلید `-u` برای `Ansible` ارسال می شود.می توانیم این را در دل خود نقش اجرایی قرارداد مثلا:

```yaml
---
- name: Copy debian And Install Docker Service
  hosts: Vagrant
  vars:
    - ansible_user: root
  roles:
    - role: InstallDocker
```

- **_کاربرد مهم آن در ابتدای اجرای برنامه است که توسط این کاربر اقدام به ایجاد اتصال `ssh` می کند._**
- **_نکته مهم دیگه اینکه بسته به اینکه کجا بگذاریمم،نتایج مختلف حاصل میشه،یعنی اگر جای نامناسب بگذاریم مثلا توی اجرای پیداکردن فایل در سرور مبدا سعی می کند که با اون کاربربه سرور مبدا وصل بشه_**



# دستورات فرافرامین

- لیست module های نصب شده

  ```elixir
  ansible-doc -l | grep module
  ```

  



# اجرای دستور با کیف رمز

## جهت ساخت

- جهت ساخت کیف رمز می شه از دستور `create ` برای ساخت یک فایل جدید استفاده می کرد.

  ```elixir
  ansible-vault {create|encrypt|edit|decrypt|view} --vault-id [dev|pro|anyName]@prompt /path2file.yml
  ```

  - توجه شود که یا حتما باید از `--vault-id @prompt` استفاده شود ویا از `--vault-password-file pFileName`

- می شه از دستور `encrypt` برای رمز کردن یک فایل موجود استفاده کرد.

- می شه هم با دستور `edit` فایل رمزشده قبلی رو همونطوری بصورت رمزنگاری شده باز کرد و تغییرات مورد نظر رو اعمال کرد.

**توصیه می شه که فایل حالت yml داشته باشه تا بشه چند پارامتر را درآن تعریف کرد مثلا:**

```yaml
Key: Value
Key: Value
...
```

درهمین حد کافیست

## جهت استفاده

درهنگام استفاده از آنها

- اگر فایل رمز شده تنهاشامل یک متغیر بود یعنی فقط `value` در فایل قرارداشت می توان خود متن فایل را مستقیما در `Playbook`ها استفاده کرد.

- اما اگر شامل جندین متغیر بود مثلا `root: Pass` در متن `Playbook`باید همچین حالتی نوشت:

  ```yaml
  remote_pass: "{{ root }}"
  ```

  و در خط فرمان از کلید `-e `جهت ارسال متغیرهای اضافی استفاده کرد.

  ```elixir
  ansible-playbook -i Inventory -e /path2Vault --vault-id @prompt /path2Role
  ```

  ## حرکت خفن: ایحاد فایل رمز از تو خود Ansible

  ایجاد یک فایل رمز توسط PlayBook

```yaml
---
- hosts: localhost
  gather_facts: false
  vars_prompt:
    - name: vault_pass
      prompt: Enter New Password
      private: yes
      salt_size: 8

  tasks:
    - name: Vault encrypt a given file
      expect:
        command: ansible-vault encrypt --ask-vault-pass toto.txt
        responses:
          New Vault password: "{{ vault_pass  }}"
          Confirm New Vault password: "{{ vault_pass  }}"
```



# Copy

```elixir
copy:
  src: /srv/myfiles/foo.conf
  dest: /etc/foo.conf
  owner: foo
  group: foo
  mode: u=rw,g=r,o=r
```

```elixir
file:
  src: /file/to/link/to
  dest: /path/to/symlink
  owner: foo
  group: foo
  state: link
```



# Create

- ایجاد فایل در میهمان

```elixir
file:
  path: /tmp/devops_directory
  state: directory
```

- ایجاد فابل در میزبان

  ```elixir
  file:
    path: /tmp/devops_directory
    state: directory
   delegate_to: localhost
  ```

- ارسال فایل به میهمان

  ```elixir
  
  ```

- دریافت فایل از میهمان

  ```elixir
  
  ```

## Role

جهت ساخت یک نقش جدید از دستور `ansible-galaxy init RoleName` استفاده می کنیم .

- اگربخواهیم بصورت استاندارداصلی کار کنیم باید وظایف را داخل `RoleName/tasks/main.yml`بنویسیم.
- می توان از استاندارد دومی استفاده کرد که آن فایل `RoleName.yml` است.
- **چرا اینها مهم هستند؟**: بدلیل اینکه فقط در این دو حالت است که می توان از چیزهای تعریف شده در غالب `Ansible` مثل شاخه `vars,tasks,files` و اینها بدون نیاز به آدرس دهی استفاده کرد والا باید نکته زیر را مد نظر قراربدیم.

<div dir=rtl align=right><h4><span style="color:red">کلا توجه شود که </span></h4><h5>ما چون role را بطور کامل پیاده سازی نکردیم باید همواره  <i> مسیرها را بطور کامل بیان کنیم</i></h5></div>

مسیری بدین شرح ایجاد:

```
Share/Ansible/VhpJNDIRoles/
├── ChangePass
│   ├── GatherJNDIInfo.yml
│   ├── ReadyJNDIs4ChangePass.yml
│   └── role
│       ├── defaults
│       │   └── main.yml
│       ├── files
│       │   └── file4Search
│       ├── handlers
│       │   └── main.yml
│       ├── meta
│       │   └── main.yml
│       ├── README.md
│       ├── Results
│       ├── tasks
│       │   └── main.yml
│       ├── templates
│       ├── tests
│       │   ├── inventory
│       │   └── test.yml
│       └── vars
│           ├── GatherJNDIInfo_var.yml
│           └── main.yml

```

بعد مثلا توی `GatherJNDIInfo.yml` قسمت 

```apl
vars_files:                                                                         
  - role/vars/GatherJNDIInfo_var.yml                                                
vars:                                                                               
  wsPass: !vault |                                                                  
    $ANSIBLE_VAULT;1.1;AES256                                                       
    36616337356235333263316230633037393061616138326564343632663863353539303930333362
    3739663033326334393132666330393833313035303261380a386634316163356137303362363739
    38626532613531356534366266313032613630626333316336386538383539633061663737326134
    6437646339303933330a333439666162313732316161636438616565303837373533626138666630
    3735                                                                            

```

- درداخل فایل `GatherJNDIInfo_var.yml` در `role/vars` داریم:

  ```yaml
  - FilePath: role/files/   
  - jndilstFile: JNDIList.py
  - globalFile: global.py   
  - Dest: /home/Ansible     
  - rResultPath: /tmp/      
  - lResultPath: role/Result
  - userName: wasadmin
  ```

- جهت صدا کردن فایلهای موجود در `role/files` کافیست مانند نمونه فوق نام آنهارا بیاوریم.

- جهت خرد کردن `tasks ` به زیر `task` آنها را در مسیر خود در `role/tasks` قرار می دهیم و اینگونه فرخوانی می کنیم

  ```yaml
  - name: pull Result to Here            
    include_tasks: role/tasks/PullRes.yml
  ```

  



## Register

نحوه ثبت یک خروجی و استفاده از آن در خروجی و یا شرط مهم است

```yaml
- name: Find Main WAS userName                                      
  shell: "{{ IBMWsNone }} {{ Dest }}/{{ findMasterUser }} | tail -1"
  register: Res                                                     
                                                                    
- name: Run Script as WSAdmin                                       
  shell: "{{ IBMWsMainUser }} {{ Dest }}/{{ jndilstFile }}"         
  when: Res.stdout == "wasadmin"                                    

```



# Inventory

در دو مدل می تواندباشد، بصورت فایل INI و بصورت yml:

![JSON format](AnsibleTips.assets/Ansible-Inventory-File-4.jpg.webp)

![groups can contain IPs and hostnames](AnsibleTips.assets/Ansible-Inventory-File-7.jpg.webp)



