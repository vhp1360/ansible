<div dir="rtl" align="right" style="color:blue"><h1>بنام خدا</h1></div>

[toc]

## نمونه دستورات:
### تعریف متغیر
```jinja
{{ <variable> }} {{ name }}
```
### حلقه
حلقه بدین صورت تعریف می شود
```jinja
{% for <var> in <list> %} Loop over a list
... {% endfor %}
{% for x in range(3) %}
{{ x }} {% endfor %}
```
مثال
```jinja
  {% for i in range(1,11) %}
  	Number {{ i }}
  {% endfor %}
```
#### بسته ای کردن حلقه ها
```jinja
{% for lan, ip, mask in lans|zip(ips, masks) %}
  interface {{ lan }}
  ip address {{ ip }}{{ mask }}
{% endfor %}
```
#### متغیر loop چیست؟
```jinja
loop.index -- 1-based index
loop.index0 -- 0-based index
loop.first -- is it the first iteration
loop.last -- is it the last iteration
{{ loop.<attr> }} {{ loop.index }} Attribute of loop object
```
برای مثال:
```jinja
{# تعاریف اولیه #}
lans:  [ eth0/1, eth0/2, eth0/3 ]
ips:   [ 10.0.0.1, 20.0.0.1, 30.0.0.1]
masks: [ /24, /24, /16]
{% for lan in lans %}
  interface {{ lan }}
  ip address {{ ips[loop.index0] }}{{ masks[loop.index0] }}
{% endfor %}
```
### پالودن(Filters)
tojson -- convert to JSON
escape -- HTML escape
indent -- indent string
{{ <something> | <filter> }} {{ loop.index | filter }} Apply a filter to an expression

### آزمایش
Tests
even/odd -- check if it is even or odd (respectively)
defined -- variable is defined
divisibleby(n) -- divisible by some number
none -- is None
Language

### شرط
#### ساده
{% if <condition> %}
{% else %}
{% endif %}
{% if x is even %} It is even
{% else %} It is odd
{% endif %}
#### تودرتو
```jinja
{% if selinux_status == "enabled" %}
	"SELINUX IS ENABLED"
{% elif selinux_status == "disabled" %}
	"SELINUX IS DISABLED"
{% else %}
	"SELINUX IS NOT AVAILABLE"
{% endif %}
```
### شیء
{{ <var>.<attribute> }} Access attribute or element
or
{{ <var>["<attribute>"] }}
{{ user.name }}
or
{{ user["name"] }}
#### اعمال رشته ای
{{ <foo> ~ <bar> }} {{ name ~ " " ~ surname }} Concatenate
{{% set var = expression % }} {% set var = name ~" Set variable to be used later

## Ansible
### دسترسی به ثابتهای Ansible در jinja
```jinja
hostname={{ ansible_facts['hostname'] }}
fqdn={{ ansible_facts['fqdn'] }}
ipaddr={{ ansible_facts['default_ipv4']['address'] }}
distro={{ ansible_facts['distribution'] }}
distro_version={{ ansible_facts['distribution_version'] }}
nameservers={{ ansible_facts['dns']['nameservers'] }}
totalmem={{ ansible_facts['memtotal_mb'] }}
freemem={{ ansible_facts['memfree_mb'] }}
```
